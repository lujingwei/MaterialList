/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */


package com.dexafree.materiallistviewexample;

import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;

public class MyPopuDialog extends PopupDialog {

    private final Component.ClickedListener clickedListener;

    public MyPopuDialog(Context context, Component contentComponent, Component.ClickedListener clickedListener) {
        super(context, contentComponent);
        this.clickedListener = clickedListener;
        Component view = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_popu_dialog, null, true);
        findCommentById(view);
        setCustomComponent(view);
    }

    private void findCommentById(Component view) {
        Text tv_clear = (Text) view.findComponentById(ResourceTable.Id_tv_clear);
        Text tv_add_satrt = (Text) view.findComponentById(ResourceTable.Id_tv_add_satrt);
        Text tv_setting = (Text) view.findComponentById(ResourceTable.Id_tv_setting);
        tv_clear.setClickedListener(clickedListener);
        tv_add_satrt.setClickedListener(clickedListener);
        tv_setting.setClickedListener(clickedListener);
    }

}
