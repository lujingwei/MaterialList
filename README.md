# MaterialList

**本项目基于开源项目MaterialList 进行openharmony的移植和开发，可以通过项目标签以及github地址（ https://github.com/dexafree/MaterialList ）追踪到原项目版本**

#### 项目介绍
- 项目名称：MaterialList
- 所属系列：openharmony的第三方组件适配移植
- 功能：MaterialList是一个帮助开发者展示漂亮Card视图的功能库
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk5，DevEco Studio2.1 beta3
- 项目作者和维护人：褚鹏飞、卢经伟
- 联系方式：chupengfei050@chinasoftinc.com/lujingwei050@chinasoftinc.com
- 原项目Doc地址：https://github.com/dexafree/MaterialList
- 基线版本：MaterialList组件 release 3.2.2

#### 效果演示

![效果演示](./printscreen/materallist.png)

#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
        }
    }
}
 ```

2.在entry模块的build.gradle文件中，

 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:MaterialList:0.0.1-SNAPSHOT')
    ......  
 }
 ```
在sdk5，DevEco Studio2.1 beta3下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

MaterialList视图基于ListContainer。它的作用就像一个普通的列表视图，但是提供了与卡片交互的选项。它可以在单个或多个列表中显示卡片。

##### 步骤1
在布局中声明MaterialListView:

```xml
<DependentLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    xmlns:tools="http://schemas.huawei.com/tools"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:background_element="$color:main_background"
    tools:context=".MainActivity"
    >

    <com.dexafree.materiallist.view.MaterialListView
        ohos:id="$+id:material_listview"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:below="$id:drLayout"/>

</DependentLayout>
```

还有一些附加属性用于定义MaterialListView的列计数，所以你需要：

```xml格式

xmlns:hap="http://schemas.huawei.com/apk/res/ohos"

```

您可以使用**column_count**属性在纵向和横向模式下使用固定列计数。

```xml格式

hap:column_count="1"

```

或者可以使用**column_count_portrait**和**column_count_landscape**属性。

```xml格式

hap:column_count_portrait="1"

hap:column_count_landscape="2"

```

##### 步骤2
在代码中找到MaterialListView
```java
MaterialListView mListView = (MaterialListView) findComponentById(ResourceTable.Id_material_listview);
```
##### 步骤3
将Card添加到MaterialListView
```java
Card card = new Card.Builder(this)
                            .setTag("BASIC_IMAGE_BUTTONS_CARD")
                            .withProvider(new CardProvider())
                            .setTitle("I'm new")
                            .setDescription("I've been generated on runtime!")
                            .setDrawable(ResourceTable.Media_dog)
                            .endConfig()
                            .build()

mListView.getItemProvider().add(card);
```
还有一些Card可能会在内容和按钮之间显示分隔符:
```java
provider.setDividerVisible(true);
```

##### 点击Card
您可以添加点击事件监听
```java
mListView.addOnItemTouchListener(new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(Card card, int position) {
                LogUtil.debug("CARD_TYPE", "" + card.getTag());
            }

            @Override
            public void onItemLongClick(Card card, int position) {
                LogUtil.debug("LONG_CLICK", "" + card.getTag());
            }
        });
```

同时检查**Recovering data from the cards**部分，以便能够恢复卡的内容

##### 移除Card
我一直喜欢的一个操作就是SwipeToDismiss手势。
MaterialList提供了这个功能，为了设置对dismissing操作的回调，只需创建OnDismissCallback：
```java
mListView.setOnDismissCallback(new OnDismissCallback() {
    @Override
    public void onDismiss(Card card, int position) {
        // Do whatever you want here
    }
});
```
您还可以通过调用“card.setDismissible（true）”来配置此Card是否可被移除。

同时检查**Recovering data from the cards**部分，以便能够恢复Card的内容

##### 动画
MaterialList可调用createAnimatorProperty创建任何property动画

#### 样例
你可以clone这个项目并自己编译它（它包括一个示例）

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

火绒安全病毒安全检测通过

当前版本demo功能与原组件基本无差异

测试员：潘国庆

#### 版本迭代
- 0.0.1-SNAPSHOT

#### 版权和许可信息
```
The MIT License (MIT)

Copyright (c) 2014 Dexafree

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
