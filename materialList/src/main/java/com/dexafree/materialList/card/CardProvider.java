package com.dexafree.materiallist.card;

import com.dexafree.materiallist.ResourceTable;
import com.dexafree.materiallist.view.cardview.CardViewOhos;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

/**
 * The basic CardProvider.
 */
public class CardProvider<T extends CardProvider> extends Observable {
    private final static int DIVIDER_MARGIN_DP = 16;

    private Context mContext;
    private Card.Builder mBuilder;

    private String mTitle;
    private String mSubtitle;
    private String mDescription;
    private boolean mDividerVisible;
    private boolean mFullWidthDivider;
    private int mTitleGravity;
    private int mSubtitleGravity;
    private int mDescriptionGravity;

    private int mBackgroundColor = 0xFFFFFFFF;
    private int mTitleColor;
    private int mSubtitleColor;
    private int mDescriptionColor;
    private int mDividerColor;
    private int mDrawable;
    private String mUrlImage;

    private final Map<Integer, Action> mActionMapping = new HashMap<>();

    private OnImageConfigListener mOnImageConfigListenerListener;
    private int mLayoutId;

    /////////////////////////////////////////////////////////////////
    //
    //      Functions related to the builder pattern.
    //
    /////////////////////////////////////////////////////////////////

    /**
     * Do NOT use this method! Only for the {@code Card.Builder}!
     *
     * @param context to access the resources.
     */
    void setContext(Context context) {
        mContext = context;
        onCreated();
    }

    /**
     * Do NOT use this method! Only for the {@code Card.Builder}!
     *
     * @param builder to return the {@code Card.Builder} by {@code endConfig}.
     */
    void setBuilder(Card.Builder builder) {
        mBuilder = builder;
    }

    protected void onCreated() {
        setTitleResourceColor(ResourceTable.Color_grey_title);
        setDescriptionResourceColor(ResourceTable.Color_description_color);
    }

    /**
     * Get the context.
     *
     * @return the context.
     */
    protected Context getContext() {
        return mContext;
    }

    /**
     * End withProvider the configuration.
     *
     * @return the {@code Card.Builder}.
     */
    public Card.Builder endConfig() {
        return mBuilder;
    }

    /**
     * Notifies the Card that the content changed.
     */
    protected void notifyDataSetChanged() {
        notifyDataSetChanged(null);
    }

    /**
     * Notifies the Card that the content changed.
     * @param object notify对象
     */
    protected void notifyDataSetChanged(final Object object) {
        setChanged();
        notifyObservers(object);
    }

    /////////////////////////////////////////////////////////////////
    //
    //      Functions related to setting and getting the properties.
    //
    /////////////////////////////////////////////////////////////////


    @SuppressWarnings("unchecked")
    public T setLayout(final int layoutId) {
        mLayoutId = layoutId;
        return (T) this;
    }

    /**
     * Get the card layout as resource.
     *
     * @return the card layout.
     */
    public int getLayout() {
        return mLayoutId;
    }

    /**
     * Get the background color.
     *
     * @return the background color.
     */

    public int getBackgroundColor() {
        return mBackgroundColor;
    }

    /**
     * Set the background color withProvider an real color (e.g. {@code Color.WHITE}).
     *
     * @param color as real.
     * @return the renderer.
     */

    @SuppressWarnings("unchecked")
    public T setBackgroundColor(final int color) {
        mBackgroundColor = color;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Set the background color withProvider an resource color (e.g. {@code
     * android.R.color.white}).
     *
     * @param color as resource.
     * @return the renderer.
     */

    public T setBackgroundResourceColor(final int color) {
        try {
            return setBackgroundColor(getContext().getResourceManager().getElement(color).getColor());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get the title.
     *
     * @return the title.
     */
    public String getTitle() {
        return mTitle;
    }

    /**
     * Set the title withProvider a string resource.
     *
     * @param title to set.
     * @return the renderer.
     */

    public T setTitle(final int title) {
        return setTitle(getContext().getString(title));
    }

    /**
     * Set the title.
     *
     * @param title to set.
     * @return the renderer.
     */
    @SuppressWarnings("unchecked")
    public T setTitle(final String title) {
        mTitle = title;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Set Gravity of title.
     *
     * @param titleGravity
     * @return the renderer.
     */


    @SuppressWarnings("unchecked")
    public T setTitleGravity(final int titleGravity) {
        mTitleGravity = titleGravity;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Get the subtitle.
     *
     * @return the subtitle.
     */
    public String getSubtitle() {
        return mSubtitle;
    }

    /**
     * Set the subtitle as resource.
     *
     * @param subtitle to set.
     * @return the renderer.
     */

    public T setSubtitle(final int subtitle) {
        return setSubtitle(getContext().getString(subtitle));
    }

    /**
     * Set the subtitle.
     *
     * @param subtitle to set.
     * @return the renderer.
     */

    @SuppressWarnings("unchecked")
    public T setSubtitle(final String subtitle) {
        mSubtitle = subtitle;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Set Gravity of subtitle
     *
     * @param subtitleGravity
     * @return the renderer.
     */

    @SuppressWarnings("unchecked")
    public T setSubtitleGravity(final int subtitleGravity) {
        mSubtitleGravity = subtitleGravity;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Get the description.
     *
     * @return the description.
     */
    public String getDescription() {
        return mDescription;
    }

    /**
     * Set the description withProvider a string resource.
     *
     * @param description to set.
     * @return the renderer.
     */

    public T setDescription(final int description) {
        return setDescription(getContext().getString(description));
    }

    /**
     * Set the description.
     *
     * @param description to set.
     * @return the renderer.
     */

    @SuppressWarnings("unchecked")
    public T setDescription(final String description) {
        mDescription = description;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Set Gravity of description
     *
     * @param descriptionGravity to set.
     * @return the renderer.
     */

    @SuppressWarnings("unchecked")
    public T setDescriptionGravity(final int descriptionGravity) {
        mDescriptionGravity = descriptionGravity;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Get the divider color as int.
     *
     * @return the divider color.
     */

    public int getDividerColor() {
        return mDividerColor;
    }

    /**
     * Set the divider color as resource.
     *
     * @param color to set.
     * @return the renderer.
     */

    public T setDividerResourceColor(final int color) {
        try {
            return setDividerColor(getContext().getResourceManager().getElement(color).getColor());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Set the divider color as int.
     *
     * @param color to set.
     * @return the renderer.
     */

    @SuppressWarnings("unchecked")
    public T setDividerColor(final int color) {
        mDividerColor = color;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Get the drawable.
     *
     * @return the drawable.
     */
    public int getDrawable() {
        return mDrawable;
    }

    /**
     * Set the drawable withProvider a drawable resource.
     *
     * @param drawable to set.
     * @return the renderer.
     */

//    public T setDrawable(final int drawable) {
//        return setDrawable(Uri.parse("android.resource://" + getContext().getBundleName()
//                + "/" + drawable).toString());
//    }

    /**
     * Set the drawable. This drawable can not be configured inside of the ImageView. It will
     * directly be drawn. If the configuration of the image is necessary, use {@link
     * #setDrawable(int)} or {@link #setDrawable(String)} and {@link
     * #setDrawableConfiguration(OnImageConfigListener)} to configure the render process.
     *
     * @param drawable to set.
     * @return the renderer.
     */

    @SuppressWarnings("unchecked")
    public T setDrawable(final int drawable) {
        mDrawable = drawable;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Set the drawable withProvider a web url.
     *
     * @param urlImage to set.
     * @return the renderer.
     */

    @SuppressWarnings("unchecked")
    public T setDrawable(final String urlImage) {
        mUrlImage = urlImage;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Get the web url.
     *
     * @return the url.
     */
    public String getImageUrl() {
        return mUrlImage;
    }

    /**
     * Get the title color as int.
     *
     * @return the color.
     */

    public int getTitleColor() {
        return mTitleColor;
    }

    /**
     * Get the title gravity as int.
     *
     * @return the gravity.
     */

    public int getTitleGravity() {
        return mTitleGravity;
    }

    /**
     * Set the title color as int.
     *
     * @param color to set as int.
     * @return the renderer.
     */

    @SuppressWarnings("unchecked")
    public T setTitleColor(final int color) {
        mTitleColor = color;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Set the title color as resource.
     *
     * @param color to set as resource.
     * @return the renderer.
     */

    public T setTitleResourceColor(final int color) {
        return setTitleColor(getContext().getColor(color));
    }

    /**
     * Get the subtitle color as int.
     *
     * @return the subtitle color.
     */

    public int getSubtitleColor() {
        return mSubtitleColor;
    }

    /**
     * Get the subtitle gravity as int.
     *
     * @return the subtitle gravity.
     */
    public int getSubtitleGravity() {
        return mSubtitleGravity;
    }

    /**
     * Set the subtitle color as resource.
     *
     * @param color to set.
     * @return the renderer.
     */

    public T setSubtitleResourceColor(final int color) {
        try {
            return setSubtitleColor(getContext().getResourceManager().getElement(color).getColor());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Set the subtitle color as int.
     *
     * @param color to set.
     * @return the renderer.
     */

    @SuppressWarnings("unchecked")
    public T setSubtitleColor(final int color) {
        mSubtitleColor = color;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Get the description color as int.
     *
     * @return the color.
     */

    public int getDescriptionColor() {
        return mDescriptionColor;
    }

    /**
     * Get the description gravity as int.
     *
     * @return the gravity.
     */
    public int getDescriptionGravity() {
        return mDescriptionGravity;
    }

    /**
     * Set the description color as int.
     *
     * @param color to set.
     * @return the renderer.
     */

    @SuppressWarnings("unchecked")
    public T setDescriptionColor(final int color) {
        mDescriptionColor = color;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Set the description color as resource.
     *
     * @param color to set.
     * @return the renderer.
     */

    public T setDescriptionResourceColor(final int color) {
        try {
            return setDescriptionColor(getContext().getResourceManager().getElement(color).getColor());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Set the listener for image customizations.
     *
     * @param listener to set.
     * @return the renderer.
     */

    @SuppressWarnings("unchecked")
    public T setDrawableConfiguration(final OnImageConfigListener listener) {
        mOnImageConfigListenerListener = listener;
        return (T) this;
    }

    /**
     * Get the listener.
     *
     * @return the listener.
     */
    public OnImageConfigListener getOnImageConfigListenerListener() {
        return mOnImageConfigListenerListener;
    }

    /**
     * Get the visibility state of the divider.
     *
     * @return the visibility state of the divider.
     */
    public boolean isDividerVisible() {
        return mDividerVisible;
    }

    /**
     * Set the divider visible or invisible.
     *
     * @param visible to set.
     * @return the renderer.
     */

    @SuppressWarnings("unchecked")
    public T setDividerVisible(final boolean visible) {
        mDividerVisible = visible;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * Get the state of wideness of the divider.
     *
     * @return the wideness of the divider.
     */
    public boolean isFullWidthDivider() {
        return mFullWidthDivider;
    }

    /**
     * Set the wideness of the divider to full.
     *
     * @param fullWidthDivider to set.
     * @return the renderer.
     */

    @SuppressWarnings("unchecked")
    public T setFullWidthDivider(final boolean fullWidthDivider) {
        mFullWidthDivider = fullWidthDivider;
        notifyDataSetChanged();
        return (T) this;
    }

    /**
     * @param actionViewId
     * @param action
     * @return T
     */

    @SuppressWarnings("unchecked")
    public T addAction(final int actionViewId, final Action action) {
        mActionMapping.put(actionViewId, action);
        return (T) this;
    }

    /**
     * @param actionViewId
     * @return Action
     */

    public Action getAction(final int actionViewId) {
        return mActionMapping.get(actionViewId);
    }

    /////////////////////////////////////////////////////////////////
    //
    //      Functions for rendering.
    //
    /////////////////////////////////////////////////////////////////

    /**
     * Renders the content and style of the card to the view.
     *
     * @param view to display the content and style on.
     * @param card to render.
     */
    @SuppressWarnings("unchecked")
    public void render(final Component view, final Card card) {
        final CardViewOhos cardView = findViewById(view, ResourceTable.Id_cardView, CardViewOhos.class);
        if (cardView != null) {
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(RgbColor.fromArgbInt(getBackgroundColor()));
            shapeElement.setCornerRadius(20);
            cardView.setBackground(shapeElement);
        }

        // Title
        final Text title = findViewById(view, ResourceTable.Id_title, Text.class);
        if (title != null) {
            title.setText(getTitle());
            title.setTextColor(new Color(getTitleColor()));
            title.setTextAlignment(getTitleGravity());
        }

        // Subtitle
        final Text subtitle = findViewById(view, ResourceTable.Id_subtitle, Text.class);
        if (subtitle != null) {
            subtitle.setText(getSubtitle());
            subtitle.setTextColor(new Color(getSubtitleColor()));
            subtitle.setTextAlignment(getSubtitleGravity());
            if (getSubtitle() == null || getSubtitle().isEmpty()) {
                subtitle.setVisibility(Component.HIDE);
            } else {
                subtitle.setVisibility(Component.VISIBLE);
            }
        }

        // Description
        final Text supportingText = findViewById(view, ResourceTable.Id_supportingText, Text.class);
        if (supportingText != null) {
            supportingText.setText(getDescription());
            supportingText.setTextColor(new Color(getDescriptionColor()));
            supportingText.setTextAlignment(getDescriptionGravity());
        }

        // Image
        final Image imageView = findViewById(view, ResourceTable.Id_image, Image.class);
        if (imageView != null) {
            if (getDrawable() != 0) {
                PixelMapElement element = new PixelMapElement(getPixelMapFromRes(mContext, getDrawable()));
                setImageSize(imageView, element.getPixelMap().getImageInfo().size);
                imageView.setCornerRadius(20);
                imageView.setPixelMap(getDrawable());
//                setImageSize(imageView);
            } else {
                final RequestCreator requestCreator = Picasso.get().load(getImageUrl());
                if (getOnImageConfigListenerListener() != null) {
                    getOnImageConfigListenerListener().onImageConfigure(requestCreator);
                }
                requestCreator.into(imageView);
            }
        }


        // Divider
        final Component divider = findViewById(view, ResourceTable.Id_divider, Component.class);
        if (divider != null) {
            divider.setVisibility(isDividerVisible() ? Component.VISIBLE : Component.INVISIBLE);

            // After setting the visibility, we prepare the divider params
            // according to the preferences
            if (isDividerVisible()) {
                // If the divider has to be from side to side, the margin will be 0
                final ComponentContainer.LayoutConfig params = (ComponentContainer.LayoutConfig)
                        divider.getLayoutConfig();
                if (isFullWidthDivider()) {
                    params.setMargins(0, 0, 0, 0);
                } else {
                    int dividerMarginPx = dpToPx(DIVIDER_MARGIN_DP);
                    // Set the margin
                    params.setMargins(
                            dividerMarginPx,
                            0,
                            dividerMarginPx,
                            0
                    );
                }
            }
        }

        // Actions
        for (final Map.Entry<Integer, Action> entry : mActionMapping.entrySet()) {
            final Component actionViewRaw = findViewById(view, entry.getKey(), Component.class);
            if (actionViewRaw != null) {
                final Action action = entry.getValue();
                action.setProvider(this);
                action.onRender(actionViewRaw, card);
            }
        }
    }

    public static PixelMap getPixelMapFromRes(Context context, int res) {
        InputStream stream = null;
        try {
            stream = context.getResourceManager().getResource(res);
            ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
            sourceOptions.formatHint = "image/png";
            ImageSource imageSource = ImageSource.create(stream, sourceOptions);
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            decodingOptions.desiredSize = new Size(0, 0);
            decodingOptions.desiredRegion = new Rect(0, 0, 0, 0);
            decodingOptions.desiredPixelFormat = PixelFormat.ARGB_8888;

            return imageSource.createPixelmap(decodingOptions);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return null;
    }

    private void setImageSize(Image image, Size size) {
        float width = size.width;
        float height = size.height;
        float scale = width / height;
        image.setBindStateChangedListener(new Component.BindStateChangedListener() {
            @Override
            public void onComponentBoundToWindow(Component component) {
                int imageHeight = component.getHeight();
                int imageWidth = (int) (imageHeight * scale);
                mContext.getUITaskDispatcher().asyncDispatch(() -> image.setWidth(imageWidth));
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {

            }
        });
    }

    protected <V extends Component> V findViewById(final Component view, final int id,
                                                   final Class<V> type) {
        final Component viewById = view.findComponentById(id);
        if (viewById != null) {
            return type.cast(viewById);
        } else {
            return null;
        }
    }

    /////////////////////////////////////////////////////////////////
    //
    //      Interfaces.
    //
    /////////////////////////////////////////////////////////////////

    /**
     * The OnImageConfigListener will be called, if an image is loaded from an url to an ImageView.
     */
    public interface OnImageConfigListener {
        /**
         * An image is loaded from an url and can be customized now.
         *
         * @param requestCreator to customize the image.
         */
        void onImageConfigure(final RequestCreator requestCreator);
    }

    /////////////////////////////////////////////////////////////////
    //
    //      Helper methods.
    //
    /////////////////////////////////////////////////////////////////

    /**
     * @param dp
     * @return px
     */
    protected int dpToPx(final int dp) {
        Display displayMetrics = DisplayManager.getInstance().getDefaultDisplay(getContext()).get();
        float dpi = displayMetrics.getAttributes().densityPixels;
        return (int) Math.round(dp * dpi + 0.5f * (dp >= 0 ? 1 : -1));
    }
}
