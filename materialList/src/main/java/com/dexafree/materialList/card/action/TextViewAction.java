package com.dexafree.materiallist.card.action;

import com.dexafree.materiallist.card.Action;
import com.dexafree.materiallist.card.Card;
import com.dexafree.materiallist.card.OnActionClickListener;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.Locale;

public class TextViewAction extends Action {

    private int mActionTextColor;
    private String mActionText;

    private OnActionClickListener mListener;

    public TextViewAction(Context context) {
        super(context);
    }

    public int getTextColor() {
        return mActionTextColor;
    }

    public TextViewAction setTextColor(final int color) {
        this.mActionTextColor = color;
        notifyActionChanged();
        return this;
    }

    public TextViewAction setTextResourceColor(final int color) {
        return setTextColor(getContext().getColor(color));
    }

    public String getText() {
        return mActionText;
    }

    public TextViewAction setText(final String text) {
        this.mActionText = text;
        notifyActionChanged();
        return this;
    }

    public TextViewAction setText(final int textId) {
        return setText(getContext().getString(textId));
    }


    public OnActionClickListener getListener() {
        return mListener;
    }

    public TextViewAction setListener(final OnActionClickListener listener) {
        this.mListener = listener;
        notifyActionChanged();
        return this;
    }

    @Override
    protected void onRender(final Component view, final Card card) {
        Text textView = (Text) view;
        textView.setText(mActionText != null ? mActionText.toUpperCase(Locale.getDefault()) : null);
        textView.setTextColor(new Color(mActionTextColor));
        textView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (mListener != null) {
                    mListener.onActionClicked(view, card);
                }
            }
        });
    }
}
