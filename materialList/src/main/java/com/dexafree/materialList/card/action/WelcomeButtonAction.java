package com.dexafree.materiallist.card.action;


import com.dexafree.materiallist.card.Card;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.utils.Rect;
import ohos.app.Context;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

public class WelcomeButtonAction extends TextViewAction {
    private final Context context;

    public WelcomeButtonAction(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onRender(Component view, Card card) {
        super.onRender(view, card);

        final Text button = (Text) view;
        Element drawable = button.getAroundElements()[0];
//        drawable.setColorFilter(getTextColor(), PorterDuff.Mode.SRC_IN);
        drawable.setBounds(0, 0, 50, drawable.getHeight());
        button.setAroundElements(drawable, null, null, null);
    }

//    private Element resize(Element image, int width, int height) {
//        Bitmap b = ((BitmapDrawable) image).getBitmap();
//        Bitmap bitmapResize = Bitmap.createScaledBitmap(b, width, height, false);
//        return new BitmapDrawable(getContext().getResources(), bitmapResize);
//    }
}
