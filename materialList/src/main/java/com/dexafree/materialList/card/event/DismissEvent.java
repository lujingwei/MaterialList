package com.dexafree.materiallist.card.event;


import com.dexafree.materiallist.card.Card;

public class DismissEvent {
    private final Card mCard;

    public DismissEvent( final Card card) {
        mCard = card;
    }

    public Card getCard() {
        return mCard;
    }
}
