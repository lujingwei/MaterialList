package com.dexafree.materiallist.card;


import ohos.agp.components.Component;
import ohos.app.Context;

public abstract class Action {
    private final Context mContext;
    private CardProvider mProvider;

    public Action(final Context context) {
        mContext = context;
    }

    void setProvider(CardProvider provider) {
        mProvider = provider;
    }

    protected Context getContext() {
        return mContext;
    }

    protected void notifyActionChanged() {
        if (mProvider != null) {
            // Only notify if something changed at runtime
            mProvider.notifyDataSetChanged();
        }
    }

    protected abstract void onRender(final Component view, final Card card);
}
