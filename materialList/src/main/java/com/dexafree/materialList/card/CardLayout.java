package com.dexafree.materiallist.card;


import ohos.agp.components.AttrSet;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;

import java.util.Observable;
import java.util.Observer;

/**
 * The CardLayout is a container for a CardView and it's content.
 */
public class CardLayout extends DirectionalLayout implements Observer {
    private Card mCard;
    private boolean mObserves;

    /**
     * Creates a new CardLayout.
     *
     * @param context
     *         for the Layout.
     */
    public CardLayout(Context context) {
        super(context);
    }

    /**
     * Creates a new CardLayout.
     *
     * @param context
     *         for the Layout.
     * @param attrs
     *         for the Layout.
     */
    public CardLayout(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    /**
     * Creates a new CardLayout.
     *
     * @param context
     *         for the Layout.
     * @param attrs
     *         for the Layout.
     * @param defStyle
     *         for the Layout.
     */
    public CardLayout(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Renders the Card content and style to the layout.
     *
     * @param card
     *         to render.
     */
    public void build( final Card card) {
        mCard = card;

        if (!mObserves) {
            mCard.getProvider().addObserver(this);
            mObserves = true;
        }

        mCard.getProvider().render(this, card);
    }

    /**
     * Get the card of this layout.
     *
     * @return the card.
     */
    public Card getCard() {
        return mCard;
    }

    @Override
    public void update(final Observable observable, final Object data) {
        if(data == null) {
            build(mCard);
            ((CardProvider) observable).notifyDataSetChanged(getCard());
        }
    }
}
