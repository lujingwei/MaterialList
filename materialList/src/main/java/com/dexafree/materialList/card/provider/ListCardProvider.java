package com.dexafree.materiallist.card.provider;

import com.dexafree.materiallist.ResourceTable;
import com.dexafree.materiallist.card.Card;
import com.dexafree.materiallist.card.CardProvider;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ListContainer;
import ohos.agp.database.DataSetSubscriber;
import ohos.multimodalinput.event.TouchEvent;

public class ListCardProvider extends CardProvider<ListCardProvider> {
    private ListContainer.ItemClickedListener mOnItemSelectedListener;
    private BaseItemProvider mAdapter;

    @Override
    protected void onCreated() {
        super.onCreated();
        setLayout(ResourceTable.Layout_material_list_card_layout_ohos);
    }

    /**
     * Set the adapter of the list.
     *
     * @param adapter of the list.
     * @return the renderer.
     */
    public ListCardProvider setAdapter(BaseItemProvider adapter) {
        mAdapter = adapter;
        notifyDataSetChanged();
        return this;
    }

    /**
     * Get the adapter of the list.
     *
     * @return the adapter.
     */
    public BaseItemProvider getAdapter() {
        return mAdapter;
    }

    /**
     * Get the listener for on item click events.
     *
     * @return the listener.
     */
    public ListContainer.ItemClickedListener getOnItemClickListener() {
        return mOnItemSelectedListener;
    }

    /**
     * Set the listener for on item click events.
     *
     * @param listener to set.
     * @return the renderer.
     */
    public ListCardProvider setOnItemClickListener(ListContainer.ItemClickedListener listener) {
        this.mOnItemSelectedListener = listener;
        return this;
    }

    @Override
    public void render(final Component view, final Card card) {
        super.render(view, card);
        if (getAdapter() != null) {
            final ListContainer listView = (ListContainer) view.findComponentById(ResourceTable.Id_listView);
            listView.setScrollbarFadingEnabled(true);
            listView.setTouchEventListener((component, touchEvent) -> {
                return false;// That the gesture detection is correct
            });
            listView.setItemProvider(getAdapter());
            listView.getItemProvider().addDataSubscriber(new DataSetSubscriber() {
                @Override
                public void onChanged() {
                    super.onChanged();
                    calculateListHeight(listView);
                }
            });
            listView.setItemClickedListener(getOnItemClickListener());
            calculateListHeight(listView);
        }
    }

    private void calculateListHeight(ListContainer listView) {
        final BaseItemProvider adapter = listView.getItemProvider();

        // Calculate the height of the ListView to display all items
        int totalHeight = 0;
        for (int i = 0; i < adapter.getCount(); i++) {
            Component item = adapter.getComponent(i, null, listView);
            item.estimateSize(
                    Component.EstimateSpec.getSizeWithMode(0, Component.EstimateSpec.UNCONSTRAINT),
                    Component.EstimateSpec.getSizeWithMode(0, Component.EstimateSpec.UNCONSTRAINT)
            );
            totalHeight += item.getEstimatedHeight();
        }

        ComponentContainer.LayoutConfig params = listView.getLayoutConfig();
        params.height = totalHeight;

        listView.setLayoutConfig(params);
        listView.postLayout();
    }
}
