package com.dexafree.materiallist.card;


import com.dexafree.materiallist.card.event.DismissEvent;
import ohos.app.Context;

/**
 * A basic Card.
 */
public class Card {

    private final CardProvider mProvider;
    private Object mTag;
    private boolean mDismissible;

    /**
     * Creates a new Card.
     *
     * @param builder to get the Card data from.
     */
    private Card( final Builder builder) {
        mProvider = builder.mProvider;
        mTag = builder.mTag;
        mDismissible = builder.mDismissible;
    }

    /**
     * Get the card content.
     *
     * @return the card content.
     */

    public CardProvider getProvider() {
        return mProvider;
    }

    /**
     * Set the tag.
     *
     * @param object as tag.
     */
    public void setTag( final Object object) {
        mTag = object;
    }

    /**
     * Get the tag.
     *
     * @return the tag.
     */
    public Object getTag() {
        return mTag;
    }

    /**
     * Set the card dismissible.
     *
     * @param dismissible {@code true} to be able to remove the card or {@code false} otherwise.
     */
    public void setDismissible(final boolean dismissible) {
        mDismissible = dismissible;
    }

    /**
     * Is the card dismissible.
     *
     * @return {@code true} if the card is removeable or {@code false} otherwise.
     */
    public boolean isDismissible() {
        return mDismissible;
    }

    /**
     * Removes the card.
     */
    public void dismiss() {
        getProvider().notifyDataSetChanged(new DismissEvent(this));
    }

    /**
     * The Card Builder configures the card.
     */
    public static class Builder {

        private final Context mContext;
        private Object mTag;
        private boolean mDismissible;
        private CardProvider mProvider;

        /**
         * Creates a new Builder.
         *
         * @param context to access resources.
         */
        public Builder( final Context context) {
            mContext = context;
        }

        /**
         * Set a tag.
         *
         * @param object as tag.
         * @return 返回标签
         */

        public Builder setTag(final Object object) {
            mTag = object;
            return this;
        }

        /**
         * Set the card dismissible - it is then removable.
         * @return 是否可移除
         */

        public Builder setDismissible() {
            mDismissible = true;
            return this;
        }

        /**
         * Set the provider.
         *
         * @param content provider which handel all the content and style.
         * @param <T>     as type of {@code CardProvider}
         * @return the content provider to config.
         */

        public <T extends CardProvider> T withProvider(T content) {
            mProvider = content;
            content.setContext(mContext);
            content.setBuilder(this);
            return content;
        }

        /**
         * Builds the card.
         *
         * @return the card.
         * @throws IllegalStateException
         */

        public Card build() {
            if (mProvider == null) {
                throw new IllegalStateException("You have to define the Card Provider");
            }
            return new Card(this);
        }
    }
}
