/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */


package com.dexafree.materiallist.view.cardview;

import com.dexafree.materiallist.AttrUtils;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.BlendMode;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.render3d.components.SceneComponent;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.global.resource.WrongTypeException;
import ohos.global.systemres.ResourceTable;
import ohos.media.image.PixelMap;

import java.io.IOException;

public class CardViewOhos extends StackLayout implements Component.DrawTask, ComponentContainer.ArrangeListener {
    private static final int[] COLOR_BACKGROUND_ATTR = new int[]{16842801};
    private boolean mCompatPadding;
    private boolean mPreventCornerOverlap;
    int mUserSetMinWidth;
    int mUserSetMinHeight;
    final Rect mContentPadding;
    final Rect mShadowBounds;
    private final RectFloat roundRect = new RectFloat();
    private float roundCorner = 50;
    private final Paint maskPaint = new Paint();
    private final Paint zonePaint = new Paint();
    private final Paint strokePaint = new Paint();
    public float mStrokeWidth = 0;
    public int mStrokeColor = 0xffffffff;
//    private final CardViewDelegate mCardViewDelegate;

    public CardViewOhos(Context context) {
        this(context, (AttrSet) null);
    }

    public CardViewOhos(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public CardViewOhos(Context context, AttrSet attrs, String styleName) {
        super(context, attrs, styleName);
//        setEstimateSizeListener(this::onEstimateSize);
        this.mContentPadding = new Rect();
        this.mShadowBounds = new Rect();
//        this.mCardViewDelegate = new CardViewDelegate() {
//            private Element mCardBackground;
//
//            public void setCardBackground(Element drawable) {
//                this.mCardBackground = drawable;
//                CardViewOhos.this.setBackground(drawable);
//            }
//
//            public boolean getUseCompatPadding() {
//                return CardViewOhos.this.getUseCompatPadding();
//            }
//
//            public boolean getPreventCornerOverlap() {
//                return mPreventCornerOverlap;
//            }
//
//            public void setShadowPadding(int left, int top, int right, int bottom) {
//                CardViewOhos.this.mShadowBounds.set(left, top, right, bottom);
//                CardViewOhos.super.setPadding(left + CardViewOhos.this.mContentPadding.left, top + CardViewOhos.this.mContentPadding.top, right + CardViewOhos.this.mContentPadding.right, bottom + CardViewOhos.this.mContentPadding.bottom);
//            }
//
//            public void setMinWidthHeightInternal(int width, int height) {
//                if (width > CardViewOhos.this.mUserSetMinWidth) {
//                    CardViewOhos.super.setMinWidth(width);
//                }
//
//                if (height > CardViewOhos.this.mUserSetMinHeight) {
//                    CardViewOhos.super.setMinHeight(height);
//                }
//            }
//
//            public Element getCardBackground() {
//                return this.mCardBackground;
//            }
//
//            public Component getCardView() {
//                return CardViewOhos.this;
//            }
//        };

        int backgroundColor = AttrUtils.getColor(attrs, "backGroundColor", ResourceTable.Color_id_color_activated_transparent);

//        roundCorner = AttrUtils.getFloat(attrs, "radius", 0.0F);
        float elevation = AttrUtils.getFloat(attrs, "elevation", 0.0F);
        float maxElevation = AttrUtils.getFloat(attrs, "maxElevation", 20.0F);
        this.mCompatPadding = AttrUtils.getBoolean(attrs, "mCompatPadding", false);
        this.mPreventCornerOverlap = AttrUtils.getBoolean(attrs, "mPreventCornerOverlap", true);
        int defaultPadding = AttrUtils.getInteger(attrs, "defaultPadding", 5);
        this.mContentPadding.left = AttrUtils.getInteger(attrs, "leftPadding", defaultPadding);
        this.mContentPadding.top = AttrUtils.getInteger(attrs, "topPadding", defaultPadding);
        this.mContentPadding.right = AttrUtils.getInteger(attrs, "rightPadding", defaultPadding);
        this.mContentPadding.bottom = AttrUtils.getInteger(attrs, "bottomPadding", defaultPadding);
        if (elevation > maxElevation) {
            maxElevation = elevation;
        }

        this.mUserSetMinWidth = AttrUtils.getInteger(attrs, "minWidth", 0);
        this.mUserSetMinHeight = AttrUtils.getInteger(attrs, "minHeight", 0);
//        IMPL.initialize(this.mCardViewDelegate, context, backgroundColor, radius, elevation, maxElevation);

        maskPaint.setAntiAlias(true);
        maskPaint.setBlendMode(BlendMode.DIFFERENCE);
        maskPaint.setStyle(Paint.Style.FILL_STYLE);
        maskPaint.setColor(Color.GREEN);
        //
        zonePaint.setAntiAlias(true);
        zonePaint.setColor(Color.YELLOW);
        //
        strokePaint.setAntiAlias(true);
        strokePaint.setColor(Color.BLUE);
        strokePaint.setStrokeWidth(10);

//        addDrawTask(this, BETWEEN_BACKGROUND_AND_CONTENT);
//        setArrangeListener(this::onArrange);
    }

    public void setPadding(int left, int top, int right, int bottom) {
    }

    public void setPaddingRelative(int start, int top, int end, int bottom) {
    }

    public boolean getUseCompatPadding() {
        return this.mCompatPadding;
    }


    public void setMinimumWidth(int minWidth) {
        this.mUserSetMinWidth = minWidth;
        super.setMinWidth(minWidth);
    }

    public void setMinimumHeight(int minHeight) {
        this.mUserSetMinHeight = minHeight;
        super.setMinHeight(minHeight);
    }

    @Override
    public void addDrawTask(DrawTask task) {
        super.addDrawTask(task);
        task.onDraw(this, mCanvasForTaskUnderContent);
    }


    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (roundCorner > 0) {
            canvas.saveLayer(roundRect, zonePaint);
            canvas.drawRoundRect(roundRect, roundCorner, roundCorner, zonePaint);
            canvas.saveLayer(roundRect, maskPaint);
//            super.draw(canvas);
            canvas.restore();

            RectFloat roundRect2 = new RectFloat();
            roundRect2.fuse(roundRect.left + mStrokeWidth / 2, roundRect.top + mStrokeWidth / 2, roundRect.right - mStrokeWidth / 2, roundRect.bottom - mStrokeWidth / 2);
            canvas.drawRoundRect(roundRect2, roundCorner, roundCorner, strokePaint);
        }
    }

    @Override
    public boolean onArrange(int i, int i1, int i2, int i3) {
        int w = getWidth();
        int h = getHeight();
        roundRect.fuse(0, 0, w, h);
        return false;
    }
}
