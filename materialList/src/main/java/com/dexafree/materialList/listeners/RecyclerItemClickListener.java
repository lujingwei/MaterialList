package com.dexafree.materiallist.listeners;


import com.dexafree.materiallist.card.Card;
import com.dexafree.materiallist.card.CardLayout;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.List;


// From http://stackoverflow.com/a/26196831/1610001
public class RecyclerItemClickListener implements ListContainer.ItemClickedListener {

    private ListContainer mRecyclerView;

    public interface OnItemClickListener {
        void onItemClick(final Card card, int position);

        void onItemLongClick(final Card card, int position);
    }

    private OnItemClickListener mListener;
    private GestureDetector mGestureDetector;

    public RecyclerItemClickListener(Context context, OnItemClickListener listener) {
        mListener = listener;

        mGestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(TouchEvent e) {
                return true;
            }

            @Override
            public void onLongPress(TouchEvent e) {
                CardLayout childView = (CardLayout) findChildViewUnder(mRecyclerView, e.getPointerScreenPosition(0).getX(),
                        e.getPointerScreenPosition(0).getY());

                if (childView != null && mListener != null) {
                    mListener.onItemLongClick(childView.getCard(), mRecyclerView.getIndexForComponent(childView));
                }
            }
        });
    }

//    @Override
//    public boolean onInterceptTouchEvent(ListContainer view, TouchEvent e) {
//        CardLayout childView = (CardLayout) findChildViewUnder(view, e.getPointerScreenPosition(0).getX(),
//                e.getPointerScreenPosition(0).getY());
//        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
//            mListener.onItemClick(childView.getCard(), view.getIndexForComponent(childView));
//        }
//
//        return false;
//    }

    @Override
    public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
        CardLayout childView = (CardLayout) component;
//        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
//        mListener.onItemClick(childView.getCard(), listContainer.getIndexForComponent(childView));
        mListener.onItemClick(childView.getCard(), i);
//        }
    }

    public void setRecyclerView(ListContainer recyclerView) {
        mRecyclerView = recyclerView;
    }

//    @Override
//    public void onRequestDisallowInterceptTouchEvent(boolean b) {
//    }


    public Component findChildViewUnder(ListContainer listContainer, float x, float y) {
        int count = listContainer.getChildCount();

        for (int i = count - 1; i >= 0; --i) {
            Component child = listContainer.getComponentAt(i);
            float translationX = child.getTranslationX();
            float translationY = child.getTranslationY();
            if (x >= (float) child.getLeft() + translationX && x <= (float) child.getRight()
                    + translationX && y >= (float) child.getTop() + translationY && y <= (float) child.getBottom() + translationY) {
                return child;
            }
        }

        return null;
    }
}