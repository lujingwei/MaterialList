package com.dexafree.materiallist.listeners;

public class ComponentConfiguration {

    private static final int DEFAULT_LONG_PRESS_TIMEOUT = 500;

    private static final int TOUCH_SLOP = 8;

    private static final int TAP_TIMEOUT = 100;

    private static final int DOUBLE_TAP_TIMEOUT = 300;

    private static final int DOUBLE_TAP_MIN_TIME = 40;

    private static final int DOUBLE_TAP_SLOP = 100;

    private static final int MINIMUM_FLING_VELOCITY = 50;

    private static final int MAXIMUM_FLING_VELOCITY = 8000;

    private static final int DOUBLE_TAP_TOUCH_SLOP = TOUCH_SLOP;

    private static final float AMBIGUOUS_GESTURE_MULTIPLIER = 2f;

    public static int getDoubleTapTimeout() {
        return DOUBLE_TAP_TIMEOUT;
    }

    public static int getDoubleTapMinTime() {
        return DOUBLE_TAP_MIN_TIME;
    }

//    public static int getTouchSlop() {
//        return TOUCH_SLOP;
//    }

    public static int getLongPressTimeout() {
        return DEFAULT_LONG_PRESS_TIMEOUT;
    }

    public static int getTapTimeout() {
        return TAP_TIMEOUT;
    }

//    public static int getDoubleTapSlop() {
//        return DOUBLE_TAP_SLOP;
//    }

//    public static int getMinimumFlingVelocity() {
//        return MINIMUM_FLING_VELOCITY;
//    }

//    public static int getMaximumFlingVelocity() {
//        return MAXIMUM_FLING_VELOCITY;
//    }

    public static int getScaledTouchSlop() {
        return TOUCH_SLOP;
    }

    public static int getScaledDoubleTapTouchSlop() {
        return DOUBLE_TAP_TOUCH_SLOP;
    }

    public static int getScaledDoubleTapSlop() {
        return DOUBLE_TAP_SLOP;
    }

    public static int getScaledMinimumFlingVelocity() {
        return MINIMUM_FLING_VELOCITY;
    }

    public static int getScaledMaximumFlingVelocity() {
        return MAXIMUM_FLING_VELOCITY;
    }

    public static float getAmbiguousGestureMultiplier() {
        return AMBIGUOUS_GESTURE_MULTIPLIER;
    }
}
