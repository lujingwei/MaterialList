package android.database;

import java.util.ArrayList;

public abstract class Observable<T> {
    protected final ArrayList<T> mObservers = null;

    public Observable() {
        throw new RuntimeException("Stub!");
    }

    public void registerObserver(T observer) {
        throw new RuntimeException("Stub!");
    }

    public void unregisterObserver(T observer) {
        throw new RuntimeException("Stub!");
    }

    public void unregisterAll() {
        throw new RuntimeException("Stub!");
    }
}
